<?php

namespace funjob\Helper;

/**
 * 周期助手
 */
class CycleTime
{

    /**
     * 获取指定长期时间戳范围
     */
    static function allCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('', $shift, $data, $now);
    }

    /**
     * 获取指定日期时间戳范围
     */
    static function dayCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('day', $shift, $data, $now);
    }

    /**
     * 获取指定星期时间戳范围
     */
    static function weekCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('week', $shift, $data, $now);
    }

    /**
     * 获取指定月份时间戳范围
     */
    static function monthCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('month', $shift, $data, $now);
    }

    /**
     * 获取指定季度时间戳范围
     */
    static function quarterCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('quarter', $shift, $data, $now);
    }

    /**
     * 获取指定半年时间戳范围
     */
    static function halfCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('half', $shift, $data, $now);
    }

    /**
     * 获取指定年度时间戳范围
     */
    static function yearCycle($data = null, $shift = 0, $now = false)
    {
        return self::getCycleTime('year', $shift, $data, $now);
    }

    /**
     * 获取时间戳范围
     * 
     * @param array $cycle 周期 day:天，week:周，month:月，quarter:季，half:半年，year:年，其他:截止今天，
     * @param date  $data 当前日期，空值为今天
     * @param int   $shift 偏移
     * @param bool  $now 是否使用当前时间
     * @return array
     */
    static function getCycleTime($cycle = '', $shift = 0, $data = null, $now = false)
    {
        $shift = (int) $shift; //偏移
        $data = $data ? $data : date('Y-m-d'); //日期
        $time = strtotime($data); // 默认时间
        $year = date("Y", $time); // 默认年份
        $month = date("m", $time); // 默认月份
        $day = date("d", $time); // 默认天份
        switch ($cycle) {
            case 'day': // 天
                $start = mktime(0, 0, 0, $month, $day + $shift, $year);
                $end = mktime(23, 59, 59, $month, $day + $shift, $year);
                break;
            case 'week': // 周
                $fistweek = 1; // 第一天
                $week = date('w', $time);
                $week = $week ? $week - $fistweek  : $fistweek + 5;
                $fistday = $day - $week + $shift * 7;
                $start = mktime(0, 0, 0, $month, $fistday, $year);
                $end = mktime(23, 59, 59, date('m', $start), date('d', $start) + 6, date('Y', $start));
                break;
            case 'month': // 月
                $start = mktime(0, 0, 0, $month + $shift, 1, $year);
                $end = mktime(23, 59, 59, date('m', $start), date("t", $start), date('Y', $start));
                break;
            case 'quarter': // 季度
                $quarter = ceil((date('n', $time) + $shift * 3) / 3);
                $fistmonth = $quarter * 3 - 2;
                $start = mktime(0, 0, 0, $fistmonth, 1, $year);
                $endtime = strtotime("+2 month", $start);
                $end = mktime(23, 59, 59, date("m", $endtime), date("t", $endtime), date('Y', $endtime));
                break;
            case 'half': // 半年
                $half = ceil((date('n', $time) + $shift * 6) / 6);
                $fistmonth = $half * 6 - 5;
                $start = mktime(0, 0, 0, $fistmonth, 1, $year);
                $endtime = strtotime("+5 month", $start);
                $end = mktime(23, 59, 59, date("m", $endtime), date("t", $endtime), date('Y', $endtime));
                break;
            case 'year': // 年
                $lastday = date("t", $time);
                $start = mktime(0, 0, 0, 1, 1, $year + $shift);
                $end = mktime(23, 59, 59, 12, 31, $year + $shift);
                break;
            default: // 长期
                $start = 0;
                $end = time();
                break;
        }
        // 是否截止当前时间
        if ($now) {
            $end = mktime(date('H'), date('i'), date('s'), date('m', $end), date('d', $end), date('Y', $end));
        }
        return [$start, $end];
    }
}
